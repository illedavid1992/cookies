package tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.Cookie;
import org.testng.annotations.Test;

import java.util.Date;

public class CookiesTests extends BaseUITests {


    @Test
    public void addCookieFromCode() {
        driver.get(hostname + "/cookie");
        SeleniumUtils.printCookies(driver);
        System.out.println("Set cookie!");
        Cookie myCookie = new Cookie("David's cookie", "20-10-2022", "/", new Date(2022, 3, 5));
        driver.manage().addCookie(myCookie);
        System.out.println("-----Show cookies after adding it------");
        SeleniumUtils.printCookies(driver);
        driver.manage().deleteAllCookies();
        SeleniumUtils.printCookies(driver);
    }


}
