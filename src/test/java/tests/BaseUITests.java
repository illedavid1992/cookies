package tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.Properties;

public class BaseUITests {
    WebDriver driver;
    //    String url = "http://86.121.249.150:4999/";
    String hostname;
    String browser;

    @AfterClass(alwaysRun = true)
    public void close() {
//      inchide browser-ul
        if (driver != null)
            driver.quit();
    }

    @BeforeClass
    public void setUpBasics() throws IOException {
        Properties prop = SeleniumUtils.readProperties("src\\test\\resources\\framework.properties");

        //read default values from config file
        hostname = prop.getProperty("hostname");
        System.out.println("Use next hostname:" + hostname);
        browser = prop.getProperty("browser");
        System.out.println("Use as default browser:" + browser);
        driver = SeleniumUtils.getDriver(browser);
    }

    protected void enterValuesOnInput(WebElement el, String value) {
        el.click();
        el.clear();
        el.sendKeys(value);
    }

    @AfterMethod
    public void saveScreenShotAtFailure(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            //your screenshooting code goes here
            String testName = result.getMethod().getMethodName();
            System.out.println("Take screen shoot..for test:" + testName);
            SeleniumUtils.takeScreenShot(driver, testName);
        }
    }
}
